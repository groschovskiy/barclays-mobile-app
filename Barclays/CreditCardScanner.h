//
//  CreditCardScanner.h
//  Barclays
//
//  Created by Groschovskiy Dmitriy on 05.06.15.
//  Copyright (c) 2015 Google Inc. All rights reserved.
//

//
//  Payment Provider - Barclays Bank and Stripe
//

#import <UIKit/UIKit.h>

@interface CreditCardScanner : UIViewController

@end
